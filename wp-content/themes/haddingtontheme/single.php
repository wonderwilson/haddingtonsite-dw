<?php get_header(); ?>


<section id="section2">

<div class="container">
  
  	<div class="row">

	<div class="col-md-8 col-md-offset-2" style="margin-top:100px; margin-bottom: 100px;">
    
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
    <p>
    
    <div class="featured-image"><?php the_post_thumbnail( 'page-feature' ); ?></div>
    
    <h1 style="margin-bottom:20px;"><?php the_title() ?></h1>
    
    <div class="date"><?php the_time('l, F jS, Y'); ?></div>
        
        <?php the_content() ?>
        
      
    </p>

      <?php endwhile; else: ?>
      <p>
        <?php _e('Sorry, there are no posts.'); ?>
      </p>
      <?php endif; ?>
    
    
    </div>
    
    </div>
    
  </div>

</section>

<?php get_footer(); ?>