 <?php
/*
Template Name: Homepage

*/
?>

<?php get_header(); ?>
<section id="section-text">



<img src="<?php bloginfo('template_url');?>/images/hidden-toun.png" class="img-responsive"/> 


</section>


<section id="section1">
	
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
    <div class="fill" style="background-image:url('<?php bloginfo('template_url');?>/images/slide-demo1.jpg');"></div>
      <div class="carousel-caption">
        <h2>Haddington in the mist</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>
      </div>
    </div>
    <div class="item">
    <div class="fill" style="background-image:url('<?php bloginfo('template_url');?>/images/slide-demo1.jpg');"></div>
      <div class="carousel-caption">
        <h2>Haddington in the mist</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>
      </div>
    </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
    
    
 </section> 

<section id="section2">

<div class="container">
  
	<div class="row">

		<div class="col-md-12" style="margin-top:28px; margin-bottom: 28px;">

			<div class="grid">
				<div class="row">
					<div class="col-md-4">
                    
                    <ul class="grid cs-style-1">
                        <li>
                            <figure>
                                <div class="q-title"><h3>Events</h3></div>
                                <img src="<?php bloginfo('template_url');?>/images/event-pic.jpg" alt="img02"/>
                                <figcaption style="text-align:left">
                                	<h3 class="event-month">July</h3>
                                    <a href="#" style="background: none; border-radius: 0px; display: inherit; padding: 0px;  text-align: inherit;">
                                    <div class="event">
                                    		<div class="events-date">
                                            28
                                            </div>
                                            <div class="event-details">
                                            <span class="events-text">An Event Title</span>
                                            <span class="events-location">St Mary's Church</span> <span class="events-cost">Free</span>
                                            </div>
                                    </div>
                                    </a>
                                    
                                    <a href="#" style="background: none; border-radius: 0px; display: inherit; padding: 0px;  text-align: inherit;">
                                    <div class="event">
                                    		<div class="events-date">
                                            28
                                            </div>
                                            <div class="event-details">
                                            <span class="events-text">An Event Title</span>
                                            <span class="events-location">St Mary's Church</span> <span class="events-cost">Free</span>
                                            </div>
                                    </div>
                                    </a>
                                    
                                    <a href="#" style="background: none; border-radius: 0px; display: inherit; padding: 0px;  text-align: inherit;">
                                    <div class="event">
                                    		<div class="events-date">
                                            28
                                            </div>
                                            <div class="event-details">
                                            <span class="events-text">An Event Title</span>
                                            <span class="events-location">St Mary's Church</span> <span class="events-cost">Free</span>
                                            </div>
                                    </div>
                                    </a>
                                    
                                    <a href="#" style="background: none; border-radius: 0px; display: inherit; padding: 0px;  text-align: inherit;">
                                    <div class="event">
                                    		<div class="events-date">
                                            28
                                            </div>
                                            <div class="event-details">
                                            <span class="events-text">An Event Title</span>
                                            <span class="events-location">St Mary's Church</span> <span class="events-cost">Free</span>
                                            </div>
                                    </div>
                                    </a>
                                    
                                    <a href="#" style="background: none; border-radius: 0px; display: inherit; padding: 0px;  text-align: inherit;">
                                    <div class="event">
                                    		<div class="events-date">
                                            28
                                            </div>
                                            <div class="event-details">
                                            <span class="events-text">An Event Title</span>
                                            <span class="events-location">St Mary's Church</span> <span class="events-cost">Free</span>
                                            </div>
                                    </div>
                                    </a>
                                    <a href="#">More Events</a>
                                </figcaption>
                            </figure>
                        </li>
                    </ul>
                    
                
					</div>
					
					<div class="col-md-8">
						<div class="row">
                        	<div class="col-md-6">
                            
                            		<ul class="grid cs-style-1">
                                        <li>
                                            <figure>
                                                <div class="q-title"><h3 style="font-size:28px;">Eating/Drinking</h3></div>
                                                        <img src="<?php bloginfo('template_url');?>/images/1.jpg" alt="img14"/>
                                                <figcaption>
                                                    <p>A brief bit of text in here describing this section of the website...</p>
                                                    <a href="#">More info</a>
                                                </figcaption>
                                            </figure>
                                        </li>
                                        
                                        <li>
                                            <figure>
                                                <div class="q-title"><h3 style="font-size:28px;">Shopping/Markets</h3></div>
                                                        <img src="<?php bloginfo('template_url');?>/images/2.jpg" alt="img14"/>
                                                <figcaption>
                                                    <p>A brief bit of text in here describing this section of the website...</p>
                                                    <a href="#">More info</a>
                                                </figcaption>
                                            </figure>
                                        </li>
                                        
                                    </ul>
                            
                            	</div>
                                
                                <div class="col-md-6">
                                    <ul class="grid cs-style-1">
                                        <li>
                                            <figure>
                                                <div class="q-title"><h3 style="font-size:28px;">Things to see/do</h3></div>
                                                        <img src="<?php bloginfo('template_url');?>/images/3.jpg" alt="img14"/>
                                                <figcaption>
                                                    <p>A brief bit of text in here describing this section of the website...</p>
                                                    <a href="#">More info</a>
                                                </figcaption>
                                            </figure>
                                        </li>
                                        
                                        <li>
                                            <figure>
                                                <div class="q-title"><h3 style="font-size:28px;">History</h3></div>
                                                        <img src="<?php bloginfo('template_url');?>/images/4.jpg" alt="img14"/>
                                                <figcaption>
                                                    <p>A brief bit of text in here describing this section of the website...</p>
                                                    <a href="#">More info</a>
                                                </figcaption>
                                            </figure>
                                        </li>
                                        
                                    </ul>
                                    
                            	</div>
                                
                                
                                
                                
                            </div>
                           
		</div>
	</div>
</div>

</section>

<section id="section3">

<div class="container">
  
	<div class="row">

		<div class="col-md-6">
        	<h3>Featured Business</h3>
        	<div class="business-featured">
            	<div class="business-featured-image">
                	<img src="<?php bloginfo('template_url');?>/images/test-business.jpg" class="img-responsive" style="width:100%; height:auto"/>
                </div>
                <div class="business-featured-text">
                 <h3 style="color:#005d29; margin:0px 0px 10px;">Mike's Bikes</h3>
                 <p>Mike’s Bikes is Haddington’s local bicycle specialist and offers sales, parts and repairs.</p>
                 
                 <table width="100%" border="0">
                  <tbody>
                    <tr>
                      <td rowspan="3" align="left" valign="top"><i class="fa fa-map-marker" aria-hidden="true"></i></td>
                      <td rowspan="3" align="left" valign="top">Mon to Sat <br>9.00am to 5.00pm</td>
                      <td align="left" valign="top"><i class="fa fa-map-marker" aria-hidden="true"></i></td>
                      <td align="left" valign="top">3, High Street, Haddington,<br>East Lothian EH41 3ES</td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><i class="fa fa-phone" aria-hidden="true"></i></td>
                      <td align="left" valign="top">01620 825 931 </td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><i class="fa fa-desktop" aria-hidden="true"></i></td>
                      <td align="left" valign="top">www.mikesbikes.com</td>
                    </tr>
                  </tbody>
                </table>
                 
                </div>
                
                  
                
            </div>
        </div>
        
        <div class="col-md-6">
        	<h3>Featured Image</h3> 
        	<div class="gallery-featured">
            <h4>Haddington in bloom</h4> 
            <a href="#">View Gallery</a>
            </div>
        </div>
        
 	</div>
    
 </div>
 
 </section>

<?php get_footer(); ?>