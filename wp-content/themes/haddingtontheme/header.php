  <head>
    <meta charset="utf-8">
    <title>Haddington - The hidden toun</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Bootstrap -->
    <link href="<?php bloginfo('template_url');?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Styles -->
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">
    <link href="<?php bloginfo('template_url');?>/css/custom.css" rel="stylesheet">
    <link src="fonts/alexfont.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script src="<?php bloginfo('template_url');?>/js/modernizr.custom.js"></script>

	<!-- Scripts -->
    <?php wp_enqueue_script("jquery"); ?>
    <?php wp_head(); ?>
  </head>
  <body>
  
  
  <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a id="haddingtonLogo" class="navbar-brand" style="padding: 0;margin: 0;padding-top: 0px;padding-right:0px;outline: 0;" href="<?php echo site_url(); ?>">
								<img id="haddingtonLogo" class="img-responsive" src="<?php bloginfo('template_url');?>/images/logo.png"/></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          
          <form class="navbar-form navbar-right" role="search">
            <div class="form-group">
              <input class="form-control" placeholder="Search" type="text">
            </div>
            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
          </form>
          
          <ul class="nav navbar-nav navbar-right">
            <li><a class="btn btn-default social" style="margin: 8px 0px; margin-left: 10px; padding: 6px 9px;" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a class="btn btn-default social" style="margin: 8px 5px; padding: 6px 9px;" href="#"><i class="fa fa-twitter" aria-hidden="true"></i>
</a></li>
            
          </ul>
          
          <ul class="nav navbar-nav navbar-right top">
            <li><a href="#">Home</a></li>
            <li><a href="#">History</a></li>
            <li><a href="#">Gallery</a></li>
            
          </ul>
          
        </div><!-- /.navbar-collapse -->
        
        <hr style="margin-bottom: 0;margin-top: 0;">
        
         <ul class="nav navbar-nav navbar-left">
            <li><a href="#">Shops and markets</a></li>
            <li><a href="#">Eating and drinking</a></li>
            <li><a href="#">Events</a></li>
            <li><a href="#">Things to see and do</a></li>
            <li><a href="#">Visitors</a></li>
            <li><a href="#">Location</a></li>
            <li><a href="#">Links</a></li>
            
          </ul>
        
      </div><!-- /.container-fluid -->
    </nav>
  
  
  
  

